// vim: et=1 ts=4 ft=zig
const wl = @import("wl.zig");
const wp = @import("wp.zig");
const std = @import("std");
const ContentBuf = u32;
const RectAt = struct {
    x: i32, y: i32, w: i32, h: i32,
    const Empty = @This(){.x = 0, .y = 0, .w = 0, .h = 0};
};
const SurfaceGeo = struct {
    source: RectAt,
    dest: RectAt,
    const Diff = packed struct {pos: bool, source: bool, dest_size: bool};
    fn diff(self: *const @This(), _other: ?@This()) Diff {
        var result = Diff{.pos = true, .source = true, .dest_size = true};
        if (_other) |other| {
            if (other.dest.x == self.dest.x and other.dest.y == self.dest.y) result.pos = false;
            if (other.dest.w == self.dest.w and other.dest.h == self.dest.h) result.dest_size = false;
            if (std.meta.eql(other.source, self.source)) result.source = false;
        }
        return result;
    }
};
const Content = struct {
    buf: ContentBuf,
    geo: SurfaceGeo,
};
const GeoMap = std.AutoArrayHashMapUnmanaged(ContentBuf, SurfaceGeo);
const Allocator = std.mem.Allocator;
pub fn wl_update(alloc: Allocator, outbuf: *wl.WBuf, subsurface: wp.wl_subsurface, viewport: wp.wp_viewport, prev: SurfaceGeo, geo: SurfaceGeo) !void {
    const diff = geo.diff(prev);
    // _ = diff;
    _ = .{viewport, alloc, outbuf, subsurface};
    if (diff.pos) try wl.bufWriteMsg(alloc, outbuf, wp.Request{.wl_subsurface = .{.obj = subsurface, .msg = .{.set_position = .{.x = geo.dest.x, .y = geo.dest.y}}}});
    if (diff.dest_size) try wl.bufWriteMsg(alloc, outbuf, wp.Request{.wp_viewport = .{.obj = viewport, .msg = .{.set_destination = .{.x = geo.dest.x, .y = geo.dest.y}}}});
    if (diff.source) try wl.bufWriteMsg(alloc, outbuf, wp.Request{.wp_viewport = .{.obj = viewport, .msg = .{.set_source = .{.x = geo.source.x, .y = geo.source.y, .width = geo.source.w, .height = geo.source.h}}}});
}
pub fn main() !void {
    const alloc = std.heap.page_allocator;
    var outbuf = wl.WBuf{};
    const geo = SurfaceGeo{.source = RectAt.Empty, .dest = RectAt.Empty};
    const geo2 = geo;
    try wl_update(alloc, &outbuf, .{.id=123}, .{.id=1234}, geo, geo2);
}
