const std = @import("std");
const wp = @import("wp.zig");
const wl = @import("wl.zig");
const WlGlobal = @TypeOf((wp.wl_registry.Event{ .global = undefined }).global);
const Conn = wl.ConnSimple(wp.Object);
pub fn try_bind(conn: *Conn, comptime Binds: type, binds: *Binds, comptime ty: type, registry: wp.wl_registry, global: WlGlobal) error{ Overflow, OutOfMemory }!void {
    if (std.mem.eql(u8, global.interface.data, ty.Name)) {
        std.log.debug("binding global: {s} {}", .{ global.interface.data, global.version });
        const new_id = try conn.idmap.client.new_id(ty.TypeCase, conn.alloc);
        try registry.write_bind(conn.alloc, &conn.outbuf, global.name, wl.Object{ .id = new_id, .iface_name = global.interface.data, .iface_ver = global.version });
        @field(binds, ty.Name) = ty{ .id = new_id };
    }
}
pub fn try_bind_types(conn: *Conn, comptime Binds: type, binds: *Binds, comptime types: []const type, registry: wp.wl_registry, global: WlGlobal) error{ Overflow, OutOfMemory }!void {
    inline for (types) |t| {
        try try_bind(conn, Binds, binds, t, registry, global);
    }
}
pub fn handle_basic(conn: *Conn, event: wp.Event) !void {
    switch (event) {
        .wl_display => |ev| switch (ev.msg) {
            .@"error" => |msg| std.log.err("wayland error: {s}", .{msg.message.data}),
            .delete_id => |msg| {
                _ = .{ conn, msg };
                // TODO: conn.idmap.remove(msg.id);
            },
        },
        else => {},
    }
}
pub fn bind_rt(conn: *Conn, comptime Binds: type, binds: *Binds, comptime types: []const type) !void {
    const display = wp.wl_display{ .id = 1 };
    const reg = try display.write_get_registry(conn.alloc, &conn.idmap, &conn.outbuf);
    const sync_cb = try display.write_sync(conn.alloc, &conn.idmap, &conn.outbuf);
    try conn.flushout();
    while (true) {
        const ev = try conn.read_event(wp.Event) orelse break;
        defer conn.inbuf.move(ev.content, ev.fds);
        try handle_basic(conn, ev.parsed);
        switch (ev.parsed) {
            .wl_registry => |rev| switch (rev.msg) {
                .global => |global| {
                    std.log.debug("compositor has global: {s} {}", .{ global.interface.data, global.version });
                    try try_bind_types(conn, Binds, binds, types, reg, global);
                },
                else => return error.WaylandUnexpected,
            },
            .wl_callback => |cev| {
                if (cev.msg == .done and cev.obj.id == sync_cb.id) {
                    try conn.flushout();
                    return;
                } else {
                    return error.WaylandUnexpected;
                }
            },
            else => return error.WaylandUnexpected,
        }
    }
}
