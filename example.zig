const wl = @import("wl.zig");
const wp = @import("wp.zig");
const wlhi = @import("wlhi.zig");
const std = @import("std");
const net = std.net;
const Binds = struct {
    wl_compositor: ?wp.wl_compositor = null,
    wp_single_pixel_buffer_manager_v1: ?wp.wp_single_pixel_buffer_manager_v1 = null,
    wp_viewporter: ?wp.wp_viewporter = null,
    xdg_wm_base: ?wp.xdg_wm_base = null,
    wl_shm: ?wp.wl_shm = null,
};

pub fn main() !void {
    const pagealloc = std.heap.page_allocator;
    const sock = try net.connectUnixSocket("/run/user/1000/wayland-0");
    var conn = wl.ConnSimple(wp.Object){ .sock = sock.handle, .alloc = pagealloc };
    var binds: Binds = .{};
    try wlhi.bind_rt(&conn, Binds, &binds, &[_]type{
        wp.wl_compositor,
        wp.wp_single_pixel_buffer_manager_v1,
        wp.wl_shm,
        wp.wp_viewporter,
        wp.xdg_wm_base,
    });
    const compositor = binds.wl_compositor orelse @panic("no wayland compositor global");
    // const spbm = binds.wp_single_pixel_buffer_manager_v1 orelse @panic("no 1pixel buf mgr");
    // const viewporter = binds.wp_viewporter orelse @panic("no viewporter");
    const shell = binds.xdg_wm_base orelse @panic("no xdg_wm_base shell");
    const wl_shm = binds.wl_shm orelse @panic("no wl_shm");

    const poolsize = 100 * 100 * 4;
    const imgfd = try std.os.memfd_create("buf", 0);
    defer std.os.close(imgfd);
    const imgobj = std.fs.File{.handle = imgfd};
    const imgw = imgobj.writer();
    for (0..10000) |_| {
        try imgw.writeAll("\xff\xff\x00\xff");
    }

    // const buffer = try spbm.write_create_u32_rgba_buffer(conn.alloc, &conn.idmap, &conn.outbuf, 255, 255, 255, 255);
    const pool = try wl_shm.write_create_pool(conn.alloc, &conn.idmap, &conn.outbuf, .{ .fd = imgfd }, poolsize);
    const buffer = try pool.write_create_buffer(conn.alloc, &conn.idmap, &conn.outbuf, 0, 100, 100, 400, wp.wl_shm.format.xrgb8888);
    const surface = try compositor.write_create_surface(conn.alloc, &conn.idmap, &conn.outbuf);
    try surface.write_attach(conn.alloc, &conn.outbuf, buffer, 0, 0);
    const xdg_surface = try shell.write_get_xdg_surface(conn.alloc, &conn.idmap, &conn.outbuf, surface);
    _ = try xdg_surface.write_get_toplevel(conn.alloc, &conn.idmap, &conn.outbuf);
    // const viewport = try viewporter.write_get_viewport(conn.alloc, &conn.idmap, &conn.outbuf, surface);
    // try viewport.write_set_destination(conn.alloc, &conn.outbuf, 100, 100);
    try surface.write_commit(conn.alloc, &conn.outbuf);

    // const display = wp.wl_display{.id=1};
    while (true) {
        try conn.flushout();
        const res = try conn.read_event(wp.Event) orelse break;
        defer conn.inbuf.move(res.content, res.fds);
        switch (res.parsed) {
            .wl_display => |ev| {
                switch (ev.msg) {
                    .@"error" => |e| {
                        std.log.err("{s}", .{e.message.data});
                    },
                    else => std.log.debug("unhandled {s}", .{@tagName(ev.msg)}),
                }
            },
            .xdg_wm_base => |ev| try ev.obj.write_pong(conn.alloc, &conn.outbuf, ev.msg.ping.serial),
            else => std.log.debug("unhandled event from {s}", .{@tagName(res.parsed)}),
        }
    }
    const left = conn.inbuf.content.items.len;
    if (left != 0) std.log.warn("{} bytes left to parse", .{left});
}
