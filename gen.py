#! /usr/bin/env python3
import sys
import xml.etree.ElementTree as ET
import json
from functools import partial
from operator import itemgetter
# https://gitlab.freedesktop.org/wayland/wayland/-/blob/main/src/scanner.c

def partlist(f, ls):
    xs, ys = [], []
    for l in ls: (xs if f(l) else ys).append(l)
    return xs, ys
def children(node, fns):
    members = {'@type': node.tag} | {f: [] for f in fns.keys()} # dict.fromkeys([]) would use a shared list!!! 
    for child in node:
        if child.tag == "description":
            members |= {"description": child.text, "summary": child.attrib['summary']}
        elif child.tag == "copyright":
            members |= {"copyright": child.text}
        else:
            members[child.tag].append(fns[child.tag](child))
    return members
def attch(**fns):
    return lambda node: node.attrib | children(node, fns)
def getattrib(node): return node.attrib
enum = attch(entry=getattrib)
msg = attch(arg=getattrib)
iface = attch(enum=enum, request=msg, event=msg)
def parse_file(file):
    root = ET.parse(file).getroot()
    assert root.tag == "protocol"
    return attch(interface=iface)(root)
def strwhen(s, cond): return s if cond else ""
def unlines(lines): return '\n'.join(lines)
def prefixlines(prefix, text):
    if text == "": return []
    else: return [x for line in text.strip('\n').split('\n') for x in ([prefix, line, '\n'] if line != "" else ['\n'])]
tab = "\t"
indentstr = partial(prefixlines, tab)
def doc(decl):
    if doc := decl.get('description') or decl.get('summary'):
        return "/// " + doc.replace('\n', "\n/// ") + "\n"
    else:
        return ""
def zident(s):
    if s in ["error", "async", "export", "type"] or s[0].isdigit(): return "@" + json.dumps(s)
    else: return s
def zname(decl, /, prefix=''): return zident(prefix+decl['name'])
def forname(begin, sep, end, f, decls, /, prefix='', write_doc=True): return ''.join([s for decl in decls for s in [strwhen(doc(decl), write_doc), begin, zname(decl, prefix=prefix), sep, f(decl), end]])
forfield = partial(forname, "", ": ", ",\n")
forinit = partial(forname, ".", " = ", ",\n")
forconst = partial(forname, "const ", " = ", ";\n")
forpubconst = partial(forname, "pub const ", " = ", ";\n")
def ses(s, e, prefix, content): return ''.join([prefix, " ", s, *(["\n", *indentstr(content)] if content != "" else []), e])
curlys, rounds = partial(ses, '{', '}'), partial(ses, '(', ')')
#def typebl(head, consts, fields): curlys()
def gen_zig(proto):
    ztypes = dict(int="i32", uint="u32", fixed="wl.Fixed", fd="wl.Fd", string="wl.String", array="[]const u8")
    def zigtype(arg):
        t = arg['type']
        if enum := arg.get("enum"):
        	return enum
        elif t in ['object', 'new_id']:
            # TODO: how strwhen("?", arg.get('allow_null') == 'true') if packed_struct?
            if iface := arg.get('interface'): return iface
            else: return "wl.Object" if t == 'new_id' else 'packed struct {id: u32}'
        elif t in ztypes: return ztypes[t]
        else: f'@compileError({json.dumps("unknown type " + t)})'
    def matchfield(args, empty, f, moreh):
        match args:
            case []: return empty
            case [one]: return f(one)
            case more: return curlys(moreh, forfield(f, more))
    def msgfn(msg):
        args = msg['arg']
        retargs, passedargs = partlist(lambda arg: arg['type'] == 'new_id' and arg.get('interface') != None, args)
        fnargs = f"self: @This(), alloc: std.mem.Allocator, {strwhen('idmap: *wl.IdMap(std.meta.Tag(Object)),', len(retargs) > 0)} outbuf: *wl.WBuf,\n" + forfield(zigtype, passedargs, prefix="field_")
        sig = f"pub fn {zname(msg, prefix='write_')}(\n{''.join(indentstr(fnargs))}) !{matchfield(retargs, 'void', zigtype, 'struct')}"
        kind = "server" if msg['@type'] == 'event' else "client"
        # TODO: if no interface
        body = ''.join([
            # const field_${new_example} = <make a new id and insert it into the map>; 
            forconst(lambda arg: curlys(zigtype(arg), f'.id = try idmap.{kind}.new_id(.{zident(arg["interface"])}, alloc)'), retargs, write_doc=False, prefix='field_'),
            "try outbuf.write(alloc, self.id, @This().", msg['@type'].capitalize(),"{.", zname(msg), " = .{",
            *[s for arg in args for s in [tab, ".", zname(arg), " = ", zname(arg, prefix="field_"), ",\n"]],
            '}}); return ', matchfield(retargs, "void{}", lambda arg: zname(arg, prefix="field_"), "."), ';'])
        return curlys(sig, body) + '\n'
    def msgenum(msgs):
        return curlys("enum(u16)", ",\n".join([*map(zname, msgs), '_']))
    def msgunion(msgs, enum):
    	# if len(msgs) == 0: return "void" # https://github.com/ziglang/zig/issues/18767
    	return curlys(f"union({enum})",
        	forfield(lambda msg: curlys("struct", forfield(zigtype, msg['arg'])), msgs)
    	)
    def msgdecls(msgs, kind):
        return f"pub const {kind}Type = {msgenum(msgs)};\npub const {kind} = {msgunion(msgs, kind + 'Type')};\n"
    # TODO: bitfield
    return forpubconst(lambda iface: curlys("struct", ''.join([
        "pub const TypeCase: std.meta.Tag(Object) = .", zname(iface), ';\n' 
        "pub const Name = ", json.dumps(iface['name']), ';\n' 
        "id: u32,\n",
        forpubconst(lambda e: curlys("enum(u32)",
            forname("", " = ", ",\n", itemgetter('value'), e['entry'])
        ), iface['enum']), "\n",
        msgdecls(iface['request'], 'Request'),
        msgdecls(iface['event'], 'Event'),
        *map(msgfn, iface['request'])
    ])), proto['interface'])
print('const wl = @import("wl.zig");')
print('const std = @import("std");')
[_, *files] = sys.argv
alliface = []
for file in files:
    print("//", file)
    wdict = parse_file(file)
    print(gen_zig(wdict))
    alliface += wdict['interface']
def allmsg(alliface, what): return curlys("union(enum)", forfield(lambda x: "struct { obj: " + zname(x) + ", msg: " + zname(x) + "." + what +" }", alliface))
print("pub const Object = " + curlys("union(enum)", forfield(zname, alliface)) + ";")
print("pub const Request = " + allmsg(alliface, "Request") + ";")
print("pub const Event = " + allmsg(alliface, "Event") + ";")

