// filetype=zig
const std = @import("std");
const native_endian = @import("builtin").target.cpu.arch.endian();
const debug = std.debug;
const Allocator = std.mem.Allocator;
const assert = std.debug.assert;
const readInt = std.mem.readInt;
pub const Fd = struct { fd: i32 };
pub const Fixed = struct { x256: u32 };
pub const String = struct { data: []const u8 };
pub const Object = struct { iface_name: []const u8, iface_ver: u32, id: u32 };
fn pad4(len: usize) usize {
    return if (len % 4 != 0) ((len / 4 + 1) * 4) - len else 0;
}
fn writeseq(w: anytype, data: []const u8, comptime nul: enum { write_nul, dont_nul }) !void {
    const send_len = data.len + (if (nul == .write_nul) 1 else 0);
    const len32 = std.math.cast(u32, send_len) orelse return error.Overflow;
    try w.writeInt(u32, len32, native_endian);
    try w.writeAll(data);
    if (nul == .write_nul) try w.writeByte(0);
    try w.writeByteNTimes(0, pad4(send_len));
}
fn writeNewObj(w: anytype, value: Object) !void {
    try writeseq(w, value.iface_name, .write_nul);
    try w.writeInt(u32, value.iface_ver, native_endian);
    try w.writeInt(u32, value.id, native_endian);
}
fn writeStruct(alloc: Allocator, w: anytype, fds: ?*ALU(i32), msg: anytype) !void {
    const Msg = @TypeOf(msg);
    const msgInfo = @typeInfo(Msg);
    const tag = std.meta.activeTag(msg);
    inline for (msgInfo.Union.fields) |ecase| {
        if (std.mem.eql(u8, ecase.name, @tagName(tag))) {
            const inner = @field(msg, ecase.name);
            inline for (@typeInfo(@TypeOf(inner)).Struct.fields) |field| {
                const value = @field(inner, field.name);
                try switch (field.type) {
                    Fd => if (fds) |_fds| _fds.append(alloc, value.fd),
                    u32, i32 => w.writeInt(field.type, value, native_endian),
                    Fixed => w.writeInt(u32, value.x256, native_endian),
                    String => writeseq(w, value.data, .write_nul),
                    []const u8 => writeseq(w, value, .dont_nul),
                    Object => writeNewObj(w, value),
                    else => if (@typeInfo(field.type) == .Enum)
                        w.writeInt(u32, @intFromEnum(value), native_endian)
                    else
                        w.writeInt(u32, value.id, native_endian),
                };
            }
        }
    }
}
fn moveArrayList(comptime T: type, al: anytype, n: usize) void {
    _ = T;
    const prevlen = al.items.len;
    for (0..(al.items.len - n)) |i| {
        al.items[i] = al.items[i + n];
    }
    al.shrinkRetainingCapacity(al.items.len - n);
    assert(al.items.len == prevlen - n);
}
test "moveArrayList" {
    const alloc = std.testing.allocator;
    const AString = std.ArrayList(u8);
    var s = AString.init(alloc);
    defer s.deinit();
    try s.appendSlice("hello");
    moveArrayList(u8, &s, 3);
    debug.assert(std.mem.eql(u8, s.items, "lo"));
}
const NullWriter = struct {
    pub const Error = error{};
    pub fn write(self: @This(), bytes: []const u8) Error!usize {
        _ = self;
        return bytes.len;
    }
};
pub const WBuf = struct {
    content: std.ArrayListAlignedUnmanaged(u8, 4) = .{},
    fds: std.ArrayListUnmanaged(i32) = .{},
    pub fn clear(self: @This()) void {
        self.content.clear();
        self.fds.clear();
    }
    pub fn move(self: *@This(), content: usize, fds: usize) void {
        moveArrayList(u8, &self.content, content);
        moveArrayList(i32, &self.fds, fds);
    }
    pub fn write(self: *@This(), alloc: Allocator, id: u32, msg: anytype) !void {
        const w = self.content.writer(alloc);
        var emptyw: std.io.CountingWriter(NullWriter) = .{ .bytes_written = 0, .child_stream = .{} };
        try w.writeInt(u32, id, native_endian);
        try writeStruct(alloc, emptyw.writer(), null, msg);
        const len64 = emptyw.bytes_written + 8;
        const len = std.math.cast(u16, len64) orelse return error.Overflow;
        const opcode: u16 = @intFromEnum(msg);
        try w.writeInt(u16, opcode, native_endian);
        try w.writeInt(u16, len, native_endian);
        try writeStruct(alloc, w, &self.fds, msg);
        // debug.print("to write {}: {any}\n", .{len, self.content.items});
    }
};
pub fn bufWriteMsg(alloc: Allocator, buf: *WBuf, msg: anytype) !void {
    switch (msg) {
        inline else => |_msg| return buf.write(alloc, _msg.obj.id, _msg.msg)
    }
}
fn eat32(data: *BufData) !u32 {
    if (data.len < 4) return error.InvalidData;
    const x = readInt(u32, data.*[0..4], native_endian);
    data.* = data.*[4..];
    return x;
}
const BufData = []align(4) const u8;
fn parseField(comptime t: type, data: *BufData, fds: *[]const i32) !t {
    switch (t) {
        u32 => return eat32(data),
        i32 => return @bitCast(try eat32(data)),
        Fixed => return .{ .x256 = try eat32(data) },
        String => {
            const sdata = try parseField([]const u8, data, fds);
            return .{ .data = if (sdata.len > 0) sdata[0..(sdata.len - 1)] else &.{} }; // cut nul
        },
        []const u8 => {
            const alen = try eat32(data);
            const padded = alen + pad4(alen);
            debug.assert(padded % 4 == 0);
            if (data.len < padded) return error.NotEnoughData;
            const adata = data.*[0..alen];
            data.* = @alignCast(data.*[padded..]);
            return adata;
        },
        Fd => {
            const fd = fds.*[0];
            fds.* = fds.*[1..];
            return .{ .fd = fd };
        },
        Object => {
            return .{
                .iface_name = (try parseField(String, data, fds)).data,
                .iface_ver = try parseField(u32, data, fds),
                .id = try parseField(u32, data, fds),
            };
        },
        else => {
            if (@typeInfo(t) == .Enum) {
                return @enumFromInt(try eat32(data));
            } else {
                return .{ .id = try eat32(data) };
            }
        },
    }
}
fn parseStruct(comptime t: type, data: BufData, fds: []const i32) !t {
    var val: t = undefined;
    var vdata = data;
    var vfds = fds;
    inline for (@typeInfo(t).Struct.fields) |f| {
        // std.log.debug("parsing field {s}\n", .{f.name});
        @field(val, f.name) = try parseField(f.type, &vdata, &vfds);
    }
    if (vdata.len != 0) return error.InvalidData;
    return val;
}
fn countFds(comptime t: type) usize {
    var n: usize = 0;
    inline for (@typeInfo(t).Struct.fields) |f| {
        if (f.type == Fd) {
            n += 1;
        }
    }
    return n;
}
pub fn ParsedData(comptime content: type) type {
    return struct { content: usize, fds: usize, parsed: content };
}
pub fn parse(comptime types: type, comptime ObjectUnion: type, data: BufData, fds: []const i32, ids: *const IdMap(std.meta.Tag(ObjectUnion))) !ParsedData(types) {
    if (data.len < 8) return error.NotEnoughData;
    const id = readInt(u32, data[0..4], native_endian);
    // TODO: big endian
    const opcode: u16 = readInt(u16, data[4..6], native_endian);
    const len = readInt(u16, data[6..8], native_endian);
    const otype = ids.lookup(id) orelse return error.ObjectNotFound;
    // yes this is len for the whole message incl header
    if (len < 8) return error.InvalidData;
    if (data.len < len) return error.NotEnoughData;
    var nfds: usize = undefined;
    inline for (@typeInfo(types).Union.fields, 0..) |objf, obji| {
        if (@intFromEnum(otype) == obji) {
            const val = @unionInit(types, objf.name, msg: {
                const Mtype = std.meta.FieldType(objf.type, .msg);
                inline for (@typeInfo(Mtype).Union.fields, 0..) |opf, opi| {
                    if (opi == opcode) {
                        nfds = countFds(opf.type);
                        break :msg .{ .obj = .{ .id = id }, .msg = @unionInit(Mtype, opf.name, try parseStruct(opf.type, data[8..len], fds)) };
                    }
                }
                return error.InvalidData;
            });
            return .{ .content = len, .fds = nfds, .parsed = val };
        }
    }
    return error.InvalidData;
}
const ALU = std.ArrayListUnmanaged;
pub fn IdMap(comptime T: type) type {
    return struct {
        pub const Half = IdMapHalf(T);
        client: Half = .{ .min = 2, .max = 0xFEFFFFFF, .last = 2 },
        server: Half = .{ .min = 0xFF000000, .max = 0xFFFFFFFF, .last = 0xFF000000 },
        pub fn lookup(self: *const @This(), id: u32) ?T {
            if (id == 1) return .wl_display;
            inline for (.{ &self.client, &self.server }) |m| {
                if (id <= m.max and id >= m.min) return m.lookup(id);
            }
            return null;
        }
    };
}
pub fn IdMapHalf(comptime T: type) type {
    return struct {
        const Map = std.AutoHashMapUnmanaged(u32, T);
        map: Map = .{},
        min: u32,
        max: u32,
        last: u32,
        pub fn new_object(self: @This(), comptime ObjectType: type, t: T, alloc: Allocator) ObjectType {
            return @unionInit(ObjectType, @tagName(t), .{ .id = self.new_id(t, alloc) });
        }
        pub fn new_id(self: *@This(), t: T, alloc: Allocator) Allocator.Error!u32 {
            // FIXME: other ids in between
            try self.map.put(alloc, self.last, t);
            self.last += 1;
            return self.last - 1;
        }
        pub fn lookup(self: *const @This(), id: u32) ?T {
            if (id == 1) return .wl_display;
            return self.map.get(id);
        }
    };
}
pub fn ConnSimple(comptime WlObject: type) type {
    return struct {
        sock: os.socket_t,
        inbuf: WBuf = .{},
        outbuf: WBuf = .{},
        idmap: IdMap(std.meta.Tag(WlObject)) = .{},
        alloc: Allocator,
        pub fn flushout(self: *@This()) !void {
            if (self.outbuf.content.items.len != 0 or self.outbuf.fds.items.len != 0) {
        	    try sendbuf(self.sock, &self.outbuf);
    	    }
        }
        pub fn read_event(self: *@This(), comptime Event: type) !?ParsedData(Event) {
            while (true) {
                return parse(Event, WlObject, self.inbuf.content.items, self.inbuf.fds.items, &self.idmap) catch |e| switch (e) {
                    error.NotEnoughData => {
                        const recved = try recvbuf(self.sock, &self.inbuf, self.alloc);
                        if (recved == 0) return null;
                        continue;
                    },
                    else => return e,
                };
            }
        }
    };
}
const os = std.os;
const net = std.net;
const socket = @import("csocket.zig");
fn ciov(data: []const u8) os.iovec_const {
    return .{ .iov_base = data.ptr, .iov_len = data.len };
}
fn miov(data: []u8) os.iovec {
    return .{ .iov_base = data.ptr, .iov_len = data.len };
}
pub const max_fds = 28; // same as libwayland
const Cmsg = extern union { msg: socket.cmsghdr, data: [socket.CMSG_SPACE(@sizeOf(i32) * max_fds)]u8 };
pub fn sendbuf(sock: os.socket_t, buf: *WBuf) !void {
    const nfds = @min(max_fds, buf.fds.items.len);
    var cmsg = std.mem.zeroes(Cmsg);
    cmsg.msg = .{
        .cmsg_level = os.SOL.SOCKET,
        .cmsg_type = socket.SCM_RIGHTS,
        .cmsg_len = socket.CMSG_LEN(@sizeOf(i32) * nfds),
    };
    const cdata: [*]i32 = @ptrCast(@alignCast(cmsg.msg.__cmsg_data()));
    @memcpy(cdata[0..nfds], buf.fds.items[0..nfds]);
    const iov = [_]os.iovec_const{ciov(std.mem.sliceAsBytes(buf.content.items))};
    const hdr = os.msghdr_const{ .name = null, .namelen = 0, .iov = &iov, .iovlen = iov.len, .control = if (nfds > 0) &cmsg.msg else null, .controllen = if (nfds > 0) @intCast(socket.CMSG_LEN(@sizeOf(i32) * nfds)) else 0, .flags = 0 };
    const sent = try os.sendmsg(sock, &hdr, 0);
    std.debug.assert(sent % 4 == 0);
    buf.move(sent, nfds);
}
fn recv(sock: os.socket_t, buf: []const os.iovec, control: *Cmsg) !usize {
    var hdr = os.msghdr{
        .name = null,
        .namelen = 0,
        .iov = @constCast(@ptrCast(buf)),
        .iovlen = @intCast(buf.len),
        .control = control,
        .controllen = @sizeOf(Cmsg),
        .flags = 0,
    };
    while (true) {
        const rc = os.linux.recvmsg(sock, &hdr, 0);
        switch (os.linux.getErrno(rc)) {
            .SUCCESS => return @as(usize, @intCast(rc)),
            .BADF => unreachable, // always a race condition
            .FAULT => unreachable,
            .INVAL => unreachable,
            .NOTCONN => return error.SocketNotConnected,
            .NOTSOCK => unreachable,
            .INTR => continue,
            .AGAIN => return error.WouldBlock,
            .NOMEM => return error.SystemResources,
            .CONNREFUSED => return error.ConnectionRefused,
            .CONNRESET => return error.ConnectionResetByPeer,
            else => return error.Unexpected,
        }
    }
}
pub fn recvbuf(sock: os.socket_t, buf: *WBuf, alloc: Allocator) !usize {
    var rbuf: [1024 * 4 * 2]u8 = undefined;
    var cmsg = std.mem.zeroes(Cmsg);
    const n = try recv(sock, &.{miov(&rbuf)}, &cmsg);
    try buf.content.appendSlice(alloc, rbuf[0..n]);
    if (cmsg.msg.cmsg_len > 0) {
        if (cmsg.msg.cmsg_type != socket.SCM_RIGHTS) return error.InvalidData;
        const cdata: [*]const i32 = @ptrCast(@alignCast(cmsg.msg.__cmsg_data()));
        const nfds = (cmsg.msg.cmsg_len - socket.CMSG_LEN(0)) / 4;
        try buf.fds.appendSlice(alloc, cdata[0..nfds]);
    }
    return n;
}
